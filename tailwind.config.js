/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {

        'mobile': {"max":'767px'},
        'tablet': {'max':'991px','min':'768px'},
        'desktop': '992px',
       
      },
    },
  },
  plugins: [],
}
