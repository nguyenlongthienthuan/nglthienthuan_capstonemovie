import React from 'react'
import { Button, Form, Input, message } from 'antd';
import { userService } from '../../../services/user.service';
import { store } from '../../..';
import { setLoading } from '../../../redux/LoadingSlice';
import { useNavigate } from 'react-router-dom';
function TabletSignInPage() {
    let navigate=useNavigate()
    const onFinish = (values) => {
        let{taiKhoan,matKhau,email,soDt,hoTen}=values;
        let param={
            taiKhoan,
            matKhau,
            email,
            soDt,
            hoTen,
            maNhom: "GP00",
           hoTen,
          }
          userService.dangki(param).then(
            (res)=>{
                navigate('/login')
                // console.log(res);
            }
          ).catch((err)=>{
            store.dispatch(setLoading(false));
            message.error(err.response.data.content)
            // console.log(err);
          })
        // console.log('Success:', values);
      };
    
      const onFinishFailed = (errorInfo) => {
        // console.log('Failed:', errorInfo);
      };
      const validateMessages = {
        required: '${label} is required!',
        types: {
          email: '${label} is not a valid email!',
          number: '${label} is not a valid number!',
          password:'${label} is not a strong pass!',
        },
        number: {
          range: '${label} must be between ${min} and ${max}',
        },
      };
      let goToLogin=()=>{
        navigate("/login")
      }
  return (
    <div className='w-screen h-screen flex justify-center items-center backGround text-center'>
        <Form
        className='w-2/3 bg-black flex justify-center items-center flex-col bg-opacity-75'
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      validateMessages={validateMessages}
      autoComplete="off"
    >     
                <h1 className='text-white text-4xl'>Đăng kí tài khoản</h1>
         <Form.Item
        name="hoTen"
        label={<b className='text-white'>full name</b>}
        rules={[
          {
            required: true,
            message: 'Please input your full name!',
            whitespace: true,
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={<b className='text-white'>User name</b>}
        name="taiKhoan"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={<b className='text-white'>Password</b>}
        name="matKhau"
        rules={[
            { 
                required: true, 
              message: 'Wrong format!',
            pattern:new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/), 
        }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        name={'email'}
        label={<b className='text-white'>Email</b>}
        rules={[
          {
            required: true,
            type: 'email',
            
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="soDt"
        label={<b className='text-white'>Phone</b>}
        rules={[
          {
            required: true,
            message: 'Please input your phone number!',
            pattern:new RegExp(/[0-9]/),
          },
        ]}
      >
        <Input
       
          style={{
            width: '100%',
          }}
        />
      </Form.Item>

      <Form.Item   wrapperCol={{ offset: 8, span: 16 }}>
      <div className='flex space-x-8'> 
        <button onClick={goToLogin} className=' text-white'>login</button>
        <Button type="primary" htmlType="submit">
          Submit
        </Button></div>
      </Form.Item>
    </Form>
    </div>
  )
}

export default TabletSignInPage