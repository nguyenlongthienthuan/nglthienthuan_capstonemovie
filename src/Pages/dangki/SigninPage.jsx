import React from 'react'

import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../HOC/Reponsive';
import DesktopSignInPage from './DeskTopSignInPage/DesktopSignInPage';
import MobileSingInPage from './mobileSignInPage/MobileSingInPage';
import TabletSignInPage from './TabletSignInPage/TabletSignInPage';
function SigninPage() {
    
  return (
  <>
   <DesktopReponsive>
    <DesktopSignInPage></DesktopSignInPage>
   </DesktopReponsive>
   <TabletReponsive>
   <TabletSignInPage></TabletSignInPage>
   </TabletReponsive>
   <MobileReponsive>
    <MobileSingInPage></MobileSingInPage>
   </MobileReponsive>
   </>
  )
 
}

export default SigninPage