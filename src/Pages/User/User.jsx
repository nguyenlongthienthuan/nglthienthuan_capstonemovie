import { Tabs } from 'antd';
import React, { useEffect, useState } from 'react'
import { userService } from '../../services/user.service'

function User() {
    const [infor,setInfor]=useState({});
    useEffect(()=>{
        userService.getThongTinTaiKhoan().then(
            (res)=>{
                // console.log(res);
                setInfor(res.data.content);
            }
        )
    },[])
  return (
    <div>
      <Tabs tabPosition='top' defaultActiveKey='tabs-user'>
           <Tabs.TabPane tab='thong tin ca nhan' key={1}>
                {
                    infor.hoTen
                }
           </Tabs.TabPane>
           <Tabs.TabPane tab='lich su dat ve' key={2}>
           <div className='container mx-auto grid grid-cols-3 gap-3  items-center'> { 
                    infor.thongTinDatVe?.map((item,index)=>{
                        return <div className='flex'>
                            <img className='w-20' src={item.hinhAnh} alt="" />
                            <div>
                                 <div>
                                       ngay dat:{item.ngayDat}<br/>
                                        rap:{item.danhSachGhe[0].tenHeThongRap+" |"+item.danhSachGhe[0].maCumRap}<br/>
                                        ghe:{item.danhSachGhe.map((rap)=>{
                                        return rap.tenGhe+','
                                        })}
                                    </div>
                                
                            </div>
                        </div>
                    })
                    
                }</div>
           </Tabs.TabPane>
      </Tabs>
    </div>
  )
}

export default User