import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import SercueView from '../../../HOC/SercueView';
import { movieService } from '../../../services/movie.service'


function PurChasePageMobile() {
    let navigate=useNavigate();
    let [datVe,setDatVe]=useState(null);
    let [choose,setChoose]=useState([]);
    let {id}=useParams();
    useEffect(()=>{
         movieService.getDatVe({MaLichChieu:id}).then(
            (res)=>{
                // console.log(res);
                setDatVe(res.data.content);
            }
         )
         
    },[])
    let clickInput=(ghe)=>{
        // console.log(ghe);
        let {maGhe,giaVe}=ghe;
        setChoose((state)=>{
            let index=state.findIndex((item)=>{return item.maGhe==maGhe})
           return state.length==0?[...state,{maGhe,giaVe}]:
           index<0?[...state,{maGhe,giaVe}]:[...state.filter((item,i)=>{if(i!=index){return item} })]
        })
        document.getElementById(maGhe).classList.toggle(`choose`)
    }
   
    let renderDanhSachGhe=useMemo(()=>{
        return datVe?.danhSachGhe.map((ghe,indexGhe)=>{
            return ghe.daDat?<input className={`gheMobile text-center gheDuocChonMobile`} key={indexGhe} value={ghe.tenGhe}  disabled/>:
                            <input id={ghe.maGhe} className={`gheMobile text-center`} onClick={()=>{clickInput(ghe)}} key={indexGhe} type="button" value={ghe.tenGhe}/>
        })
    },[datVe])
    let renderListChoose=useMemo(
        ()=>{
            return choose.map((item)=>{return `${item.maGhe},`})
        },[choose]
    )
    let renderPrice=useCallback(()=>{
        let price=0;
        choose.forEach((item)=>{ price+=item.giaVe})
        return price;
    },[choose])

    let purchase= useCallback(()=>{
            const params={
                "maLichChieu": id,
                "danhSachVe": choose,
              }
            movieService.postDatVe(params).then(
                (res)=>{
                    // console.log(res);
                    navigate('/')
                }
            )
        },[])
   
  return (
 <SercueView>
     <div className='backGround h-screen'>
        <div className='max-w-5xl mx-auto bg-white bg-opacity-40 rounded-xl p-2 h-1/6 '>
        <h1 className='text-white'>tên phim:{datVe?.thongTinPhim.tenPhim}</h1>
        <h1 className='text-white'>giờ chiếu:{datVe?.thongTinPhim.gioChieu}</h1>      
        <h1 className='text-white'>tên rạp chiếu:{datVe?.thongTinPhim.tenCumRap}</h1>
        <h1 className='text-white'>{datVe?.thongTinPhim.tenRap}</h1>
        </div>

     <div className='container max-w-xl mx-auto grid grid-cols-12 p-12 h-4/6 gap-1 text-white'>
        {renderDanhSachGhe}
    </div>
    
    <div className=' max-w-5xl mx-auto p-1 bg-slate-500 flex justify-between items-center rounded-xl h-1/6'>
       <h1 className='text-white '>Số ghế chọn {renderListChoose}</h1>
        <h1 className='text-white'>giá  {renderPrice()}</h1>
    <div><button className=' bg-blue-500 text-white px-2 py-1 border-none border-2 border-black  rounded-2xl hover:bg-blue-100 ' onClick={()=>{purchase()}}>thanh toan</button></div>
    </div>
    
     </div>
 </SercueView>
  )
}

export default PurChasePageMobile