import React from 'react'
import { Button, Form, Input } from 'antd';
import { useDispatch } from 'react-redux';
import { userService } from '../../services/user.service';
import { setUserInfo } from '../../redux/UserSlice';
import { useNavigate } from 'react-router-dom';
import { localService } from '../../services/local.service';
import { setLoading } from '../../redux/LoadingSlice';
function LoadingPage() {
  let dispatch=useDispatch()
let navigate=useNavigate()
  const onFinish = (values) => {
    // console.log(values);
    userService.get(values).then((res)=>{
      // console.log(res.data.content);
      // luu xuong local
      localService.set(res.data.content)
      dispatch(setUserInfo(res.data.content))
      navigate('/')
    }).catch((err)=>{
      console.log(err);
      dispatch(setLoading(false))
      alert(err.response.data.content)
    })
  };
  const onFinishFailed = (errorInfo) => {
    
    // console.log('Failed:', errorInfo);
  };
  let goToSignIn=()=>{
    navigate('/signin')
  }
  return (
    <div className='backGround container mx-auto flex justify-center items-center w-screen h-screen'>
      <Form
      className=' bg-black bg-opacity-20'
    name="basic"
    labelCol={{
      span: 8,
    }}
    wrapperCol={{
      span: 16,
    }}
    initialValues={{
      remember: true,
    }}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
  >
    <Form.Item
      label="taiKhoan"
      name="taiKhoan"
      rules={[
        {
          required: true,
          message: 'Please input your username!',
        },
      ]}
    >
      <Input autoFocus />
    </Form.Item>

    <Form.Item
      label="matKhau"
      name="matKhau"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
    >
      <Input.Password />
    </Form.Item>

    <Form.Item
      wrapperCol={{
        offset: 8,
        span: 16,
      }}
    >
     <div className=' space-x-8'>
      <button onClick={goToSignIn} className=' text-white'>sign in</button>
     <Button type="primary" htmlType="submit">
        Submit
      </Button>
     </div>
    </Form.Item>
  </Form>
</div>
  )
}

export default LoadingPage