import React from 'react'
import {  Card } from 'antd';
import {  useNavigate } from 'react-router-dom';
import { localService } from '../../../../services/local.service';
const { Meta } = Card;

function ItemMoviesPhanTrangTablet({data}) {
  let navigate=useNavigate()
    let {maPhim,tenPhim,moTa,trailer,hinhAnh}=data;
    let datVe=()=>{
      if(localService.get()){
      navigate(`/detailPage/${maPhim}`)
      }else{
        navigate('/login')
      }
    }
  return (
    <Card
    hoverable
    style={{ height:`100%` ,background:'black',color:'white',padding:`0`}}
    cover={<div className='h-40 w-full py-1 relative'><img className='object-contain w-full h-full' alt="example" src={hinhAnh} />
        <div className=' absolute top-0 left-0 w-full h-full bg-black bg-opacity-0 flex justify-center items-center hover:bg-opacity-50'><i class="fa-solid fa-play"></i></div>
</div>}
  >
    <Meta className='p-0' title={<div className=' text-white w-full p-0 m-0' style={{fontSize:`15px`}}>{tenPhim}</div>} description={<div className='text-white h-32  ' >{moTa.substr(0,100)+'...'}</div>}/>
   <button className='' onClick={()=>{datVe()} } style={{}}>ĐẶT VÉ</button>
  </Card>
  )
}

export default ItemMoviesPhanTrangTablet