import React, { useEffect, useState } from 'react'
import { movieService } from '../../../../services/movie.service'
import ItemMoviesPhanTrang from '../ItemMoviesPhanTrang'

function MoviesPhanTrangTablet() {
     const [movies, setmovies] = useState([])
     useEffect(() => {
         movieService.getMoviePhanTrang({soTrang:1,soPhanTuTrenTrang:10}).then(
            (res)=>{
                // console.log(res);
                setmovies(res.data.content.items)
            }
         )
     }, [])
     
     let renderList=()=>{
        return movies.map((item,index)=>{
            return <ItemMoviesPhanTrang key={index} data={item}></ItemMoviesPhanTrang>
        })
     }
  return (
    <div className='container mx-auto grid grid-cols-4 gap-4'>
        {renderList()}
    </div>
  )
}

export default MoviesPhanTrangTablet