import React from 'react'
import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../../HOC/Reponsive'
import MoviesPhanTrangMobile from './mobileRps/MoviesPhanTrangMobile'
import MoviewPhanTrangDesktop from './DesktopRps/MoviewPhanTrangDesktop'
import MoviesPhanTrangTablet from './tabletRps/MoviesPhanTrangTablet'

function MoviesPhanTrang() {
 
  return (
   <div className='p-5'>
   <DesktopReponsive>
    <MoviewPhanTrangDesktop></MoviewPhanTrangDesktop>
   </DesktopReponsive>
   <TabletReponsive>
    <MoviesPhanTrangTablet></MoviesPhanTrangTablet>
   </TabletReponsive>
   <MobileReponsive>
    <MoviesPhanTrangMobile></MoviesPhanTrangMobile>
   </MobileReponsive>
   </div>
  )
}

export default MoviesPhanTrang