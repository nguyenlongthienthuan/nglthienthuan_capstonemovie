import React, { useEffect, useState } from 'react'
import { movieService } from '../../../../services/movie.service'
import ItemMoviesPhanTrang from '../ItemMoviesPhanTrang'

function MoviewPhanTrangDesktop() {
     const [movies, setmovies] = useState([])
     useEffect(() => {
         movieService.getMoviePhanTrang({soTrang:1,soPhanTuTrenTrang:9}).then(
            (res)=>{
                console.log(res.data.content.items);
                setmovies(res.data.content.items)
            }
         )
     }, [])
     
     let renderList=()=>{
        return movies.map((item,index)=>{
            return <ItemMoviesPhanTrang key={index} data={item}></ItemMoviesPhanTrang>
        })
     }
     console.log(movies);
  return (
    <div className='container mx-auto grid grid-cols-5 gap-4'>
        {renderList()}
    </div>
  )
}

export default MoviewPhanTrangDesktop