import React from 'react'

import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../../HOC/Reponsive';
import ItemMoviesPhanTrangMobile from './mobileRps/ItemMoviesPhanTrangMobile';
import ItemMoviesPhanTrangTablet from './tabletRps/ItemMoviesPhanTrangTablet';
import ItemMoviewPhanTrangDesktop from './DesktopRps/ItemMoviewPhanTrangDesktop';
import Popup from 'reactjs-popup';
import PopUpTrailer from './PopUpTrailer';

function ItemMoviesPhanTrang({data}) {
  
  return (
  <>
   <MobileReponsive>
   <Popup modal trigger={<button><ItemMoviesPhanTrangMobile data={data}></ItemMoviesPhanTrangMobile></button>} >
   {close => <PopUpTrailer close={close} data={data}/>}
      </Popup>
   </MobileReponsive>
   <TabletReponsive>
   <Popup modal trigger={<button><ItemMoviesPhanTrangTablet data={data}></ItemMoviesPhanTrangTablet></button>} >
   {close => <PopUpTrailer close={close} data={data}/>}
      </Popup>
    {/* <ItemMoviesPhanTrangTablet data={data}></ItemMoviesPhanTrangTablet> */}
   </TabletReponsive>
   <DesktopReponsive>
   <Popup modal trigger={<button><ItemMoviewPhanTrangDesktop data={data}></ItemMoviewPhanTrangDesktop></button>} >
   {close => <PopUpTrailer close={close} data={data}/>}
      </Popup>
    {/* <ItemMoviewPhanTrangDesktop data={data}></ItemMoviewPhanTrangDesktop> */}
   </DesktopReponsive>
  </>
  )
}

export default ItemMoviesPhanTrang