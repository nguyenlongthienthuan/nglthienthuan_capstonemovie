import React from 'react'

function PopUpTrailer({close,data}) {
  // console.log("test data popup",data);
  let {tenPhim,trailer,moTa}=data;
  let getId=(url)=> {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
}
    
const videoId = getId(trailer);
const iframeMarkup = <iframe width="100%" height="315" src={`https://www.youtube.com/embed/${videoId}`}></iframe>;

console.log('Video ID:', videoId)
  return (
    <div className=' w-screen h-screen flex items-center justify-center bg-black bg-opacity-60'>
        <div className=" w-2/3  modal bg-white z-50">
    <a className="close" onClick={close}>
      &times;
    </a>
    <div className="header"> {tenPhim} </div>
    <div className="content w-full">
  {/* <iframe width="420" height="315"
src={trailer}>
</iframe> */}
{iframeMarkup}
<p>{moTa}</p>
    </div>
  </div>
    </div>
  )
}

export default PopUpTrailer