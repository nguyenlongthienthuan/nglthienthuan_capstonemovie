import React, { useEffect, useState } from 'react'
import { movieService } from '../../../../services/movie.service'
import ItemMoviesPhanTrang from '../ItemMoviesPhanTrang'

function MoviesPhanTrangMobile() {
     const [movies, setmovies] = useState([])
     useEffect(() => {
         movieService.getMoviePhanTrang({soTrang:2,soPhanTuTrenTrang:20}).then(
            (res)=>{
                // console.log(res);
                setmovies(res.data.content.items)
            }
         )
     }, [])
     
     let renderList=()=>{
        return movies.map((item,index)=>{
            return <ItemMoviesPhanTrang key={index} data={item}></ItemMoviesPhanTrang>
        })
     }
  return (
    <div className='container mx-auto grid grid-cols-4'>
        {renderList()}
    </div>
  )
}

export default MoviesPhanTrangMobile