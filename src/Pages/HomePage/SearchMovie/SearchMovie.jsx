import { Button, Select } from 'antd'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { DesktopReponsive } from '../../../HOC/Reponsive'
import { movieService } from '../../../services/movie.service'

function SearchMovie() {
    
    let navigate=useNavigate()
    const [listMovie, setlistMovie] = useState([])
    const [heThongRap, setHeThongRap] = useState({cumRap:[],lichChieuPhim:[]})
    const [lichChieu, setlichChieu] = useState([])
    const [maLichChieu, setmaLichChieu] = useState(null)
    useEffect(() => {
      movieService.getMovieList().then(
        (res)=>{
            // console.log(res.data.content);
            setlistMovie(res.data.content.map((movie)=>{return {value:movie.maPhim,label:movie.tenPhim}}));
        }
      )
    }, [])
    let fetchThongTinLichChieuPhim=useCallback((e)=>{
         movieService.getThongTinLichChieuPhim({MaPhim:e}).then(
            (res)=>{
                setHeThongRap((state)=>{
                    let arrCumRap=[];
                let lichChieu=[];
                res.data.content.heThongRapChieu.forEach((heThongRap)=>{
                     heThongRap.cumRapChieu.forEach((rap) => {
                        arrCumRap.push({value:rap.maCumRap,label:rap.tenCumRap})
                        lichChieu.push({maCumRap:rap.maCumRap,lichChieuPhim:rap.lichChieuPhim.map((lich)=>{
                            return {value:lich.maLichChieu,label:lich.ngayChieuGioChieu}
                        })}
                        )
                    });}
                    )

                    return {cumRap:arrCumRap,lichChieuPhim:lichChieu}
                    });
                // setlichChieu(lichChieu);
                renderLichchieu(null)
            }
         )
    },[])
    let renderLichchieu=useCallback((e)=>{
       let index=heThongRap.lichChieuPhim?.findIndex((lich)=>{return lich.maCumRap==e})
       if (index<0) {setlichChieu([]);setmaLichChieu(null)}
       else{
        setlichChieu(heThongRap.lichChieuPhim[index].lichChieuPhim)
        // console.log(heThongRap.lichChieuPhim[index].lichChieuPhim);
    //    return heThongRap.lichChieuPhim[index].lichChieuPhim
        }
      },[heThongRap])   
      let gotoPurchase=()=>{
        if(maLichChieu!=null){
            navigate(`/purchase/${maLichChieu}`)
        }
      }
  return (
   <DesktopReponsive>
     <div  className='container h-16 rounded-2xl border-2 text-xl absolute bottom-0 left-1/2 -translate-x-1/2 z-20 bg-white flex justify-evenly items-center'>
        <Select
        onChange={fetchThongTinLichChieuPhim}
        className="w-1/4"
      defaultValue='Chọn phim'
   
      style={{
       
      }}
      bordered={false}
      options={listMovie}
    />
        <Select
        value={heThongRap.cumRap[0]?heThongRap.cumRap[0].label:"chọn rạp"}
        onChange={(e)=>{renderLichchieu(e)}}
        className="w-1/4"
        defaultValue={"rạp"}
        style={{}}
        bordered={false}
        options={heThongRap.cumRap}
    />
        <Select
        value={lichChieu[0]?lichChieu[0].label:"chọn lịch"}
       onChange={(e)=>{setmaLichChieu(e)}}
      className="w-1/4 text-3xl"
      defaultValue={"lịch chiếu"}
      style={{
       
      }}
      bordered={false}
      options={lichChieu}
    />
    <Button className="p-3 bg-red-600 " onClick={gotoPurchase}>Đặt vé</Button>
    </div>
   </DesktopReponsive>
  )
}

export default SearchMovie