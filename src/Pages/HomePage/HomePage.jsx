import React from 'react'
import HeaderHome from '../../components/header/HeaderHome'
import { Card } from 'antd';
import 'antd/dist/antd.css';
import Banner from './banner/Banner';
import MoviesPhanTrang from './moviePhanTrang/MoviesPhanTrang';
import TabsHeThongRap from './tabs/TabsHeThongRap';
import Popup from 'reactjs-popup';
import PopUpTrailer from './moviePhanTrang/PopUpTrailer';
import Footer from '../../components/Footer/Footer';

// import React from 'react';
const { Meta } = Card;
function HomePage() {
  return (
    <div>
      <HeaderHome></HeaderHome>
       <Banner></Banner>
       <MoviesPhanTrang></MoviesPhanTrang>
       <TabsHeThongRap></TabsHeThongRap>
       <Footer></Footer>
    </div>
  )
}

export default HomePage