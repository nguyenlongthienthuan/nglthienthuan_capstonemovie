import React from 'react'

function ItemBanner({data}) {
    let {hinhAnh,maBanner,maPhim}=data
  return (
    <div >
    <img className='w-full object-contain'style={{
      height:'70vh'
    }} src={hinhAnh} alt="" />
  </div>    
  )
}

export default ItemBanner