import React, { useEffect } from 'react'
import { Carousel } from 'antd';
import { movieService } from '../../../services/movie.service';
import { setBanner } from '../../../redux/movieSlice';
import { useDispatch, useSelector } from 'react-redux';
import ItemBanner from './ItemBanner';
import SearchMovie from '../SearchMovie/SearchMovie';

function Banner() {
    let dispatch=useDispatch()
    let bannerList=useSelector((state)=>{
        return state.movieSlice.banner;
    })
    useEffect(()=>{
        movieService.getBanner().then(
            (res)=>{
                console.log("banner list",res.data.content);
              dispatch(setBanner(res.data.content))
            }
        )
    },[])
    let renderList=()=>{
        return bannerList?.map((banner,index)=>{
            return <ItemBanner key={index} data={banner} ></ItemBanner>
        })
    }
    // console.log('hi');
  return (
   <div className='w-screen bg-black relative'>
<SearchMovie></SearchMovie>
<div className='container mx-auto h-full'>

<Carousel autoplay>
  
  {renderList()}  
  
  </Carousel>
</div>
   </div>
   
  )
}

export default Banner