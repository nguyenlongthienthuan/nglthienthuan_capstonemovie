import React, { useEffect } from 'react'
import { Card, Tabs } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { movieService } from '../../../services/movie.service';
import { setLichChieuHeThongRap } from '../../../redux/movieSlice';
import ItemMoviesLichChieu from './ItemMoviesLichChieu';
import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../../HOC/Reponsive';
import TabsHeThongRapDesktop from './desktopRps/TabsHeThongRapDesktop';
import TabsHeThongRapMoblie from './mobileRps/TabsHeThongRapMoblie';
import TabsHeThongRapTablet from './tabletRps/TabsHeThongRapTablet';
function TabsHeThongRap() {
    
  return (
    <div className=' bg-black bg-opacity-50' >
     <MobileReponsive>
       <TabsHeThongRapMoblie></TabsHeThongRapMoblie>
      </MobileReponsive>
    <DesktopReponsive>
    <TabsHeThongRapDesktop></TabsHeThongRapDesktop>
    </DesktopReponsive>
   <TabletReponsive>
<TabsHeThongRapTablet></TabsHeThongRapTablet>
   </TabletReponsive>
    </div>
  )
}

export default TabsHeThongRap