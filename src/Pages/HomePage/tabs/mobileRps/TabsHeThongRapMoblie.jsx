import React, { useEffect } from 'react'
import { Card, Tabs } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { movieService } from '../../../../services/movie.service';
import { setLichChieuHeThongRap } from '../../../../redux/movieSlice';
import ItemMoviesLichChieu from '../ItemMoviesLichChieu';
import ItemMoviesLichChieuMobile from './ItemMoviesLichChieuMobile';
function TabsHeThongRapMoblie() {
  let dispatch=useDispatch();
    let HeThongRap=useSelector((state)=>{
        return state.movieSlice.lichChieuHeThongRap;
    })
    useEffect(()=>{
        movieService.getMovieHeThongRap().then(
            (res)=>{
                // console.log(res);
                dispatch(setLichChieuHeThongRap(res.data.content))
            }
        )
    },[])
    let renderList=()=>{
      return HeThongRap.map((heThongRap,index)=>{
        return   <Tabs.TabPane className='' tab={<div className='w-10 h-10 border-2 rounded-full border-yellow-300'><img className='w-10' src={heThongRap.logo}/></div>} key={index}>
                  <Tabs tabPosition={'top'} defaultActiveKey={`${index}-cumrap`}>
                  {heThongRap.lstCumRap.map((cumRap,indexCumRap)=>{
                    let {tenCumRap,hinhAnh,diaChi,danhSachPhim}=cumRap
                    return <Tabs.TabPane tab={
                      <Card
                      size="small"
                      title={tenCumRap}
                      style={{
                        width: 200,
                        borderRadius:'17px',
                        
                      }}  
                    >
                      <div className='w-full'><img className='mx-auto w-24 mb-3 rounded-xl' src={hinhAnh}/></div>
                      <marquee>{diaChi}</marquee>
                    </Card>
                    } key={indexCumRap}>
                      {danhSachPhim.map((movie,indexMovie)=>{
                        return <ItemMoviesLichChieu key={indexMovie} data={movie}></ItemMoviesLichChieu>
                      })}
                </Tabs.TabPane> 
                  })}
                  </Tabs>
              </Tabs.TabPane> 
        // <ItemMoviesLichChieu key={index} data={item}></ItemMoviesLichChieu>
      })
    }
  return (
    <div className=' max-w-5xl mx-auto overflow-auto relative p-5' style={{
      height:``
    }}>
        <Tabs  className='' tabPosition={'top'} defaultActiveKey="1-tabs">
       {renderList()}
</Tabs>;
    </div>
  )
}

export default TabsHeThongRapMoblie