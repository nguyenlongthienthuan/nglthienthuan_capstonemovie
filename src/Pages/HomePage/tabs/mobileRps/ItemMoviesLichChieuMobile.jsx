import { Button } from 'antd';
import moment from 'moment/moment';
import React from 'react'
import { NavLink } from 'react-router-dom';

function ItemMoviesLichChieuMobile({data}) {
  let {lstLichChieuTheoPhim,maPhim, tenPhim, hinhAnh, hot}=data;
  return (
    <div className='w-full h-40 border-2 mb-1 flex overflow-hidden hover:overflow-y-auto relative p-1 border-black bg-black bg-opacity-50 rounded-3xl'>
      <div className='w-1/3 h-full sticky top-0'><img  className='mx-auto h-full object-contain rounded-2xl' src={hinhAnh}/></div>
       <div className='text-center w-2/3'>
        <h2 className=' text-white'>{tenPhim}</h2>
        <div className='grid grid-cols-2 gap-2 ml-1'>
          {lstLichChieuTheoPhim.map((items)=>{
            return <NavLink to={`/purchase/${items.maLichChieu}`}><button className='bg-black hover:text-red-300 py-1 px-2 rounded-2xl text-white' style={{fontSize:`smaller`}}>{moment(items.ngayChieuGioChieu).format("DD-MM-YYYY ~ DD:HH")}</button></NavLink>
          })}
        </div>
       </div>
    </div>
  )
}

export default ItemMoviesLichChieuMobile