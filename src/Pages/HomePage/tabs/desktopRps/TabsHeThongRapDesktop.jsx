import React, { useEffect } from 'react'
import { Card, Tabs } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { movieService } from '../../../../services/movie.service';
import { setLichChieuHeThongRap } from '../../../../redux/movieSlice';
import ItemMoviesLichChieu from '../ItemMoviesLichChieu';
function TabsHeThongRapDesktop() {
  let dispatch=useDispatch();
    let HeThongRap=useSelector((state)=>{
        return state.movieSlice.lichChieuHeThongRap;
    })
    useEffect(()=>{
        movieService.getMovieHeThongRap().then(
            (res)=>{
                console.log("hethongrap",res.data.content);
                dispatch(setLichChieuHeThongRap(res.data.content))
            }
        )
    },[])
    let renderList=()=>{
      return HeThongRap.map((heThongRap,index)=>{
         console.log('listcumrap',heThongRap.lstCumRap);
        return   <Tabs.TabPane className='' tab={<div className='w-10 h-10 border-2 rounded-full border-yellow-300'><img className='w-10' src={heThongRap.logo}/></div>} key={index}>
                  <Tabs tabPosition={'left'} defaultActiveKey={`${index}-cumrap`}>
                  {heThongRap.lstCumRap.map((cumRap,indexCumRap)=>{
                    let {tenCumRap,hinhAnh,diaChi,danhSachPhim}=cumRap
                    return <Tabs.TabPane tab={
                      <Card
                      size="small"
                      title={tenCumRap}
                      style={{
                        width: 200,
                        borderRadius:10,
                      }}  
                    >
                      <div className='w-full h-28'><img  className='mx-auto h-full object-contain rounded-2xl' src={hinhAnh}/></div>
                      <marquee>{diaChi}</marquee>
                    </Card>
                    } key={indexCumRap}>
                      {danhSachPhim.map((movie,indexMovie)=>{
                        return <ItemMoviesLichChieu key={indexMovie} data={movie}></ItemMoviesLichChieu>
                      })}
                </Tabs.TabPane> 
                  })}
                  </Tabs>
              </Tabs.TabPane> 
        // <ItemMoviesLichChieu key={index} data={item}></ItemMoviesLichChieu>
      })
    }
  return (
    <div className=' max-w-5xl mx-auto overflow-auto relative p-1 border-2' style={{
      height:`60vh`
    }}>
        <Tabs  className='' tabPosition={'left'} defaultActiveKey="1-tabs">
       {renderList()}
</Tabs>;
    </div>
  )
}

export default TabsHeThongRapDesktop