import { Button } from 'antd';
import React from 'react'
import { NavLink } from 'react-router-dom';
import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../../HOC/Reponsive';
import ItemMoviesLichChieuDesktop from './desktopRps/ItemMoviesLichChieuDesktop';
import ItemMoviesLichChieuMobile from './mobileRps/ItemMoviesLichChieuMobile';
import ItemMoviesLichChieuTablet from './tabletRps/ItemMoviesLichChieuTablet';

function ItemMoviesLichChieu({key,data}) {
  let {lstLichChieuTheoPhim,maPhim, tenPhim, hinhAnh, hot}=data;
  return (
    <>
    <MobileReponsive><ItemMoviesLichChieuMobile key={key} data={data}></ItemMoviesLichChieuMobile></MobileReponsive>
    <TabletReponsive><ItemMoviesLichChieuTablet key={key} data={data}></ItemMoviesLichChieuTablet></TabletReponsive>
    <DesktopReponsive><ItemMoviesLichChieuDesktop key={key} data={data}></ItemMoviesLichChieuDesktop></DesktopReponsive>
    </>
  )
}

export default ItemMoviesLichChieu