
import { Button, Tabs } from 'antd';
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { movieService } from '../../services/movie.service'
import { Card } from 'antd';
import SercueView from '../../HOC/SercueView';
import { useMediaQuery } from 'react-responsive';
import Footer from '../../components/Footer/Footer';
import moment from 'moment';
function DetailPage() {
  // let dispatch=useDispatch()
  let navigate=useNavigate()
  const [detailMovies, setDetailMovie] = useState({})
    let {id}=useParams();
    let {moTa}=detailMovies
    let tabPosition= useMediaQuery({ maxWidth: 767 })?"top":"left";
    let widthContentTab=useMediaQuery({ maxWidth: 500 })?100:200;
    let heightContentTab=useMediaQuery({ maxWidth: 500 })?'auto':250;
    useEffect(()=>{
        // console.log(id);
        movieService.getThongTinLichChieuPhim({MaPhim:id}).then(
            (res)=>{
                // console.log(res.data.content);
                setDetailMovie(res.data.content)
            }
        )
    },[])
    let renderTrailer=(trailer)=>{

    }
    let gotoPurchase=(id)=>{
      navigate(`/purchase/${id}`)
    }
  return (
    <SercueView>
      <div className=' bg-black bg-opacity-30 h-screen p-1'>
      <div className='max-w-4xl mx-auto h-48 border-2 flex mb-10 p-1 overflow-scroll relative rounded-xl'>
      <div className=' w-2/4 sticky top-0  mr-2 bg-black'>
      <img className='w-full h-full object-contain object-center rounded-xl ' onClick={()=>{
        renderTrailer(detailMovies.trailer)
      }} src={detailMovies.hinhAnh} alt="" />
      </div>
      <div className='w-3/4'>
        <h1>{detailMovies.tenPhim}</h1>
        <p>lich chieu: {detailMovies.ngayKhoiChieu}</p>
        <p>{moTa}</p>
      </div>
     </div>
     <div className='container mx-auto max-w-4xl h-96 overflow-scroll border-2 p-1 rounded-xl'>
     <Tabs defaultActiveKey='1' tabPosition={tabPosition}>
        {
          detailMovies.heThongRapChieu?.map((heThongRap,index)=>{
            return <Tabs.TabPane tab={<div className='w-10 h-10 border-2 rounded-full border-yellow-300'><img className='w-10' src={heThongRap.logo}/></div>} key={index}>
                <Tabs tabPosition='left'>
                {heThongRap.cumRapChieu.map((cumRap,indexCumRap)=>{
                  return  <Tabs.TabPane key={indexCumRap} tab={
                    <Card size="small" title={cumRap.tenCumRap} style={{ width: widthContentTab,height:heightContentTab,borderRadius:`10px  `}}>
                    <img className='mx-auto' src={cumRap.hinhAnh} alt="" />
                    <marquee className='w-full text-center p-1'>{cumRap.diaChi}</marquee>
                     </Card>
                  }>
                      {cumRap.lichChieuPhim.map((lichChieu)=>{
                        let {maLichChieu, maRap, tenRap, ngayChieuGioChieu, giaVe}=lichChieu
                        return <button onClick={()=>{
                          gotoPurchase(maLichChieu)
                        }} className=" border-2 border-black bg-black bg-opacity-20 text-white rounded-xl mb-2">
                           {moment(ngayChieuGioChieu).format("DD-MM-YYYY ~ DD:HH")}
                        </button>
                      })}
                  </Tabs.TabPane>
                 })}
                </Tabs>
            </Tabs.TabPane>
          })
        }
     </Tabs>
     </div>
    </div>
    <Footer></Footer>
    </SercueView>
  )
}

export default DetailPage