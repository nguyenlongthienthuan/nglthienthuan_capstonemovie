import { createSlice } from "@reduxjs/toolkit"
import { localService } from "../services/local.service";

let initialState={
    userInfo:localService.get(),
}
const UserSlice=createSlice({
    name:'UserSlice',
    initialState,
    reducers:{
        setUserInfo:(state,{payload})=>{
            state.userInfo=payload;
        },
    }
})
export const {setUserInfo}=UserSlice.actions;
export default UserSlice.reducer;