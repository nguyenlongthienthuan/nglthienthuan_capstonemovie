import { createSlice } from "@reduxjs/toolkit"

const initialState={
    banner:[],
    lichChieuHeThongRap:[],
}
const movieSlice=createSlice({
    name:"movieSlice",
    initialState,
    reducers:{
        setBanner:(state,{payload})=>{
            // console.log(payload);
            state.banner=payload;
        },
        setLichChieuHeThongRap:(state,{payload})=>{
            // console.log(payload);
            state.lichChieuHeThongRap=payload;
        }
    }
})
export const{setBanner,setLichChieuHeThongRap}=movieSlice.actions;
export default movieSlice.reducer;