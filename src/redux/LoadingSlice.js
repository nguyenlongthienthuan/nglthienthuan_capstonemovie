import { createSlice } from "@reduxjs/toolkit";
let initialState={
    isLoading:false,
}
const LoadingSlice=createSlice({
    name:"LoadingSlice",
    initialState,
    reducers:{
        setLoading:(state,{payload})=>{
            state.isLoading=payload;
        }
    }
})
export const { setLoading }=LoadingSlice.actions
export default LoadingSlice.reducer