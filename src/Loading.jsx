import React from 'react'
import { useSelector } from 'react-redux'
import { CircleLoader } from 'react-spinners';

function Loading() {
    let isLoading=useSelector((state)=>{
        return state.LoadingSlice.isLoading;
    })

  return (
    isLoading? <div className=' h-screen w-screen flex justify-center items-center bg-black'>
    <CircleLoader className='' color="#36d7b7" />
        </div>:<></>
  )
}

export default Loading