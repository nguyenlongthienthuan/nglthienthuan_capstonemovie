import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoadingPage from './Pages/LoginPage/LoginPage';
import HomePage from './Pages/HomePage/HomePage';
import DetailPage from './Pages/DetailPage/DetailPage';
import PurChasePage from './Pages/PurChasePage/PurChasePage';
import User from './Pages/User/User';
import Loading from './Loading';
import SigninPage from './Pages/dangki/SigninPage';

function App() {
  return (
    <div className="">
     <Loading></Loading>
     <BrowserRouter>
       <Routes>
          <Route path="/" element={<HomePage></HomePage>}></Route>
          <Route path="/login" element={<LoadingPage></LoadingPage>}></Route>
          <Route path="/detailPage/:id" element={<DetailPage></DetailPage>}></Route>
          <Route path="/purchase/:id" element={<PurChasePage></PurChasePage>}></Route>
          <Route path="/thongtincanhan" element={<User></User>}></Route>
          <Route path="/signin" element={<SigninPage></SigninPage>}></Route>
       </Routes>
       
          
     </BrowserRouter>
    </div>
  );
}

export default App;
