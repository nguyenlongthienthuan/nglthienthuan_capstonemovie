
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink, useNavigate,  } from 'react-router-dom';
import { setUserInfo } from '../../redux/UserSlice';
import { localService } from '../../services/local.service';

function NavHeader() {
  let navigate=useNavigate()
  let dispatch=useDispatch()
    let userInfor=useSelector((state)=>{
        return state.UserSlice.userInfo;
    })
    useEffect(()=>{
      if (userInfor){
        navigate('/')
      }
    },[])
    let logout=()=>{
        dispatch(setUserInfo(null))
        localService.remove()
    }
  return (
    userInfor?
    <div className=' space-x-2'>
    <NavLink to='/thongtincanhan'><button className='bg-blue-500 py-1 px-3 rounded-2xl text-white'>{userInfor.hoTen}</button></NavLink>
    <NavLink onClick={()=>{logout()}} to="/"><button className='bg-red-500 py-1 px-3 rounded-2xl text-white'>Logout</button></NavLink>
    </div>:
    <div className=' space-x-2'>
    <NavLink to='/signin'><button   className='bg-red-500 py-1 px-3 rounded-2xl text-white'>Signin</button></NavLink>

    <NavLink to='/login'><button className='bg-blue-500 py-1 px-3 rounded-2xl text-white'>Login</button></NavLink>

    </div>
  )
}

export default NavHeader