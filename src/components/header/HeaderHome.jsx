
import React from 'react'
import NavHeader from './NavHeader'

function HeaderHome() {
  return (
    <div className=' fixed z-50 w-screen bg-black bg-opacity-80 text-white' style={{
     
    }}>
      <div className='flex  items-center justify-between p-5 shadow-2xl'>
        <div>
      <h1 className=" text-white text-2xl" style={{ fontWeight:`500`}}>THT</h1>
        </div>
        <div className="space-x-4">
            <span>HOME</span>
            <span>CONTACT</span>
            <span>NEW</span>
        </div>
        <div>
            <NavHeader></NavHeader>
        </div>
    </div>
    </div>
  )
}

export default HeaderHome