import axios from "axios"
import { store } from ".."
import { setLoading } from "../redux/LoadingSlice"
import { localService } from "./local.service"

export const baseUrl="https://movienew.cybersoft.edu.vn"
export let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAwMSIsIkhldEhhblN0cmluZyI6IjMwLzA5LzIwMzEiLCJIZXRIYW5UaW1lIjoiMTk0ODQ5MjgwMDAwMCIsIm5iZiI6MTYwMTIyNjAwMCwiZXhwIjoxOTQ4NjQwNDAwfQ.4l-eTzlgVnFczfvc2Or7BNPOcaesY3Kwc8RoNm-o-6M"
export let headerConfig=()=>{
    return{
        TokenCybersoft: token,
        Authorization:localService.get()?"Bearer "+localService.get().accessToken:"",
    }
}
// let authortoken=()=>{
//   let tokenauthor=localService.get()?"Bearer "+localService.get().accessToken:"";
//   // console.log("authortoken", token);
//   return tokenauthor
// }
export const https=axios.create({
    timeout: 300000,
    baseURL:baseUrl,
    headers:{
        TokenCybersoft: token,  
        // Authorization:authortoken(),
    }
});
// Add a request interceptor
https.interceptors.request.use(
    function (config) {
      store.dispatch(setLoading(true));
      // console.log("before axios");
      // bật loading
      // Do something before request is sent
      // window.axios.defaults.headers.common["Authorization"] = "bearer " + userInforLocal.get()?.accessToken;
      return config;
    },
    function (error) {
      // Do something with request error
      return Promise.reject(error);
    }
  );
  
  // Add a response interceptor
  https.interceptors.response.use(
    function (response) {
      // tắt loading
      store.dispatch(setLoading(false));
      // console.log("after");
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
  
      return response;
    },
    function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    }
  );
  