import axios from "axios"
import { localService } from "./local.service"
import { baseUrl, https, token } from "./url.service.config"

export const movieService={
    getBanner:()=>{
        const uri='/api/QuanLyPhim/LayDanhSachBanner'
        return https.get(uri)
    },
    getMovieList:()=>{
        const uri="/api/QuanLyPhim/LayDanhSachPhim"
        return https.get(uri)
    },
    getMoviePhanTrang:(params)=>{
     const uri="/api/QuanLyPhim/LayDanhSachPhimPhanTrang"
     return https.get(uri,params)
    },
    getMovieHeThongRap:()=>{
        const uri='/api/QuanLyRap/LayThongTinLichChieuHeThongRap';
        return https.get(uri)
    },
    getThongTinLichChieuPhim:(params)=>{
        // console.log(params);
        const uri='/api/QuanLyRap/LayThongTinLichChieuPhim';
        return https.get(uri,{params})
    },
    getDatVe:(params)=>{
        // console.log(params);
        const uri=`/api/QuanLyDatVe/LayDanhSachPhongVe`;
        return https.get(uri,{params})
    },
    postDatVe:(params)=>{
        // console.log("authortoken",localService.get().accessToken);
    //    let headers={
    //         // TokenCybersoft: token,  
    //         Authorization:"Bearer "+localService.get().accessToken,
    //     }
    let headers={
        TokenCybersoft: token,  
        Authorization:"Bearer "+localService.get().accessToken,
    }
    //   console.log(params);
          const uri=`/api/QuanLyDatVe/DatVe`;
            return axios({
                url:`${baseUrl+uri}`,
                method:'POST',
                data:params,
                headers:{
                    TokenCybersoft: token,  
                  Authorization:"Bearer "+localService.get().accessToken,
                }
            })   
    }
}