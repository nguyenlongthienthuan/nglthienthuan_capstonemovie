import axios from "axios";
import { localService } from "./local.service";
import { baseUrl, https, token } from "./url.service.config"

export let userService={
    get:(data)=>{
        let uri = "/api/QuanLyNguoiDung/DangNhap"
        return  https.post(uri,data);
    },
    dangki:(params)=>{
      let uri="/api/QuanLyNguoiDung/DangKy";
      return https.post(uri,params)
    },
    getThongTinTaiKhoan:()=>{
       let uri='/api/QuanLyNguoiDung/ThongTinTaiKhoan';
         return  axios({
        url:`${baseUrl+uri}`,
        method:'POST',
        headers:{
            TokenCybersoft: token,  
          Authorization:"Bearer "+localService.get().accessToken,
        }
    })   
    }
}